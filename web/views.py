from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def profile(request):
    return render(request, 'profile.html', {})

def exp(request):
    return render(request, 'exp.html', {'title': 'Resume'})

